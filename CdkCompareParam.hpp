struct CdkComapreParam{
	CdkComapreParam& SetNumMatches(int nm){ NumMatches = nm; return *this; }
	CdkComapreParam& SetEpsilon(double ep){ Epsilon = ep; return *this; }
	CdkComapreParam& SetIterations(int it){ Iterations = it; return *this; }
	int NumMatches; //Min number of matches for two cdks to be the same
	double Epsilon; //Iterate until diff > Epsilon or
	int Iterations; //Iterations
};