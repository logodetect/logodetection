#include "Cdk.hpp"
#include "Optimization.hpp"

void Cdk::AddPoint(const InterestPoint& kp){
	m_kps.push_back(kp);
}

void Cdk::ClearPoint(){
	m_kps.clear();
}

void Cdk::ComputeAdjecency(){
	for (vDouble::iterator it = m_thetaBins.begin(); it < m_thetaBins.end(); ++it){
		for (vDouble::iterator is = m_scaleBins.begin(); is < m_scaleBins.end(); ++is){
			vDouble::iterator nit = (it + 1 == m_thetaBins.end()) ? m_thetaBins.begin() : it + 1; //cyclic
			int d = m_kps.size();
			m_adj.emplace_back(cv::Mat(d, d, CV_32FC1));
			ComputeAdjecency(m_adj.back(), *it, *is, *nit, *is + sDelta);
		}
	}
}

bool Cdk::IsMatch(const Cdk& refCdk){
	if (CompareParamMatch(refCdk))
	{
		cv::Mat K = Optimization::FindSimilarityMatrix(*this, refCdk, m_param, 1, 1);
	}
}

//Get Set
void Cdk::SetParams(const CdkParam& param){
	m_param = param;

	tDelta = 2 * M_PI / m_param.AngleBins;
	for (int t = -1 * M_PI; t < M_PI; t += tDelta){
		m_thetaBins.push_back(t);
	}

	sDelta = 1; //1 or more
	for (int s = 0; s < m_param.ScaleBins; ++s){
		m_scaleBins.push_back(sDelta + s*sDelta);
	}
}

const vKeyPoints& Cdk::KeyPoints() const{
	return m_kps;
}

const InterestPoint& Cdk::operator[](size_t i)const{
	return m_kps[i]; //no checks
}

//Helpers
void Cdk::ComputeAdjecency(cv::Mat& adj, double tLow, double sLow, double tHigh, double sHigh){
	int d = adj.rows;
	for (size_t i = 0; i < d; ++i){
		for (size_t j = 0; j < d; ++j){
			adj.at<float>(i, j) = IsAdjacent(m_kps[i], m_kps[j], 
				tLow, sLow, tHigh, sHigh);
		}
	}
}

bool Cdk::IsAdjacent(const InterestPoint& aKp, const InterestPoint& bKp, 
	double tLow, double sLow, double tHigh, double sHigh){
	//TEST is aNkp.angle -pi to pi?
	//Measure if geo angle(bKp) is within theta & theta+1 with aKp.angle as ref!
	double bGeo = math::aDiff(bKp.pt, cv::Point(0, 0)); //geo angle
	double bGeoRel = math::aDiff(aKp.angle, bGeo); //With aKp.angle as ref
	return ((bKp.size >= aKp.size * sLow) &&
		(bKp.size < aKp.size * sHigh) &&
		math::aCmp(bGeoRel, tLow) &&
		!math::aCmp(bGeoRel, tHigh));
}

void Cdk::CheckConsistency(){

}

bool Cdk::CompareParamMatch(const Cdk& refCdk){
	return (refCdk.m_param == m_param);
}

void Cdk::ComputeMatch(const Cdk& refCdk){

}