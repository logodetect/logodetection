#ifndef CDKPARAM
#define CDKPARAM

class CdkParam{
public:
	CdkParam& SetAngleBins(int ab){ AngleBins = ab; return *this; }
	CdkParam& SetScaleBins(int sb){ ScaleBins = sb; return *this; }
	bool operator==(const CdkParam& rCdk) const{
		return (rCdk.AngleBins == AngleBins) &&
			(rCdk.ScaleBins == ScaleBins);
	}
	int AngleBins;
	int ScaleBins;
};

#endif