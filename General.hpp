#include <vector>
#include <opencv2\features2d\features2d.hpp>
#define _USE_MATH_DEFINES
#include <cmath>
#include <math.h>

typedef std::vector<cv::KeyPoint> vKeyPoints;
typedef std::vector<double> vDouble;

namespace math{
	//TEST
	//Angles in radians
	double aDiff(cv::Point a, cv::Point b){
		return atan2((b.y - a.y), (b.x - a.x));
	}

	double aDiff(double x, double y){
		return atan2(sin(x - y), cos(x - y));
	}

	double aAvg(double a, double b){
		return atan2((sin(a) + sin(b)) / 2, (cos(a) + cos(b)) / 2);
		//return (angle < 0) ? angle + 2 * M_PI : angle; //range (0 to 2*pi)
	}

	bool aCmp(double x, double y){
		return ( atan2(sin(x - y), cos(x - y)) >= 0 );
	}

	/*double mathOp::adiff(double a, double b){ //range (0 to pi)
		double temp = (cos(a)*cos(b)) + (sin(a)*sin(b));
		if (temp > 1){ //out of range corrections due to approx errors
			temp = 1;
		}
		else if (temp < -1){
			temp = -1;
		}
		return acos(temp); //from dot product
	}*/
}