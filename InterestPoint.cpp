#include "InterestPoint.hpp"

InterestPoint::InterestPoint(const cv::KeyPoint& kp, const cv::Mat& desc) : m_kp(kp), m_desc(desc){}

double InterestPoint::operator - (const InterestPoint& b) const{
	double l2norm = 0;
	if (m_desc.size() == b.Desc().size())
	{
		size_t sz = m_desc.ptr<double>.size();
		const double* x = m_desc.ptr<double>(0);
		const double* y = b.Desc().ptr<double>(0);
		for (size_t i = 0; i < sz; ++i){
			l2norm += std::sqrt(std::pow(x[i] - y[i],2)); //CHANGE abs() ?
		}
	}
	return l2norm;
}

const cv::KeyPoint& InterestPoint::KeyPoint() const{
	return m_kp;
}

const cv::Mat& InterestPoint::Desc() const{
	return m_desc;
}