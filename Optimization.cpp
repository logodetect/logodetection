#include "Optimization.hpp"

cv::Mat Optimization::FindSimilarityMatrix(const Cdk& X, const Cdk& Y,
	const CdkParam& param, double a, double b){
	size_t xsz = X.KeyPoints().size();
	size_t ysz = Y.KeyPoints().size();

	//Find diff mat
	cv::Mat D = cv::Mat(xsz, ysz, CV_32FC1);
	for (size_t i = 0; i < xsz; ++i)
	{
		for (size_t j = 0; j < ysz; ++j)
		{
			D.ptr<double>(i)[j] = X[i] - Y[i];
		}
	}

	
}