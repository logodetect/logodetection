#ifndef INTERESTPOINT
#define INTERESTPOINT

#include "General.hpp"

class InterestPoint{
public:
	InterestPoint(const cv::KeyPoint& kp, const cv::Mat& desc);
	double operator-(const InterestPoint& b) const;
	const cv::KeyPoint& KeyPoint() const;
	const cv::Mat& Desc() const;
private:
	cv::KeyPoint m_kp;
	cv::Mat m_desc;
};

#endif