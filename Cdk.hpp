#include "General.hpp"
#include "CdkParam.hpp"
#include "InterestPoint.hpp"

/*@brief : 
*	It is a bundle of interest points from same image
*	Each point will have adjecency relations with other points in the img
*	The pdf of similarity btw 2 cdks can be computed
*	Match (true/false) between 2 cdks can be computed
*/
class Cdk{
public:
	//APIs
	void AddPoint(const InterestPoint& kp);
	void ClearPoint();
	void ComputeAdjecency();
	bool IsMatch(const Cdk& refCdk);
	//Get Set
	void SetParams(const CdkParam& param);
	const vKeyPoints& KeyPoints()const;
	const InterestPoint& operator[](size_t i)const;
private:
	//Helpers
	void CheckConsistency();
	bool CompareParamMatch(const Cdk& refCdk);
	void ComputeMatch(const Cdk& refCdk);
	/*adj is a square matrix of dimension d*/
	void ComputeAdjecency(cv::Mat& adj, double tLow, double sLow, 
		double tHigh, double sHigh);
	bool IsAdjacent(const InterestPoint& aKp, const InterestPoint& bKp,
		double tLow, double sLow, double tHigh, double sHigh);

	//Members
	CdkParam m_param;
	vKeyPoints m_kps;
	std::vector<cv::Mat> m_adj;
	vDouble m_thetaBins; //-PI to PI
	vDouble m_scaleBins; //Dist factors
	double tDelta;
	double sDelta;
};