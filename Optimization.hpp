#ifndef OPTIMIZATION
#define OPTIMIZATION

#include "Cdk.hpp"

namespace Optimization
{
	cv::Mat FindSimilarityMatrix(const Cdk& X, const Cdk& Y, 
		const CdkParam& param, double a, double b);

	bool FindDifferenceMatrix();
}
#endif